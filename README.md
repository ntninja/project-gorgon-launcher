Unofficial launcher for the Linux version the **Project: Gorgon** online MMORPG game

Currently only a colored terminal UI has been implemented. If you think that
**Project: Gorgon** deserves something better, I'd welcome your contribution 😉

# Installation #

 1. Download the latest version of the launcher from here:
    - https://ipfs.io/ipns/QmZ86ow1byeyhNRJEatWxGPJKcnQKG7s51MtbHdxxUddTH/Software/Python/pglauncher/pglauncher (latest stable version, if unsure use this)
    - https://gitlab.com/alexander255/project-gorgon-launcher/repository/archive.tar.bz2 (latest GIT version)
 2. If you are using the latest GIT version make sure that you have the `requests` library for Python 3 installed
    - [Installation link for Ubuntu](https://apps.ubuntu.com/cat/applications/python3-requests/)
 3. If you want to use the Tkinter-based GUI, make sure that you have the `tk` library for Python 3 installed
   - [Installation link for Ubuntu](https://apps.ubuntu.com/cat/applications/python3-tk/)
 4. Make the downloaded file executable:
   - Right-click on the downloaded file in the file manager
   - Choose Properties from the menu
   - Open the Permission tab in the dialog
   - Check the Run file as program or File is executable (name may slightly vary) checkbox
   - Close the dialog.
 5. Run the downloaded `project-gorgon` file (by double-clicking it for instance)

*Note*: The stable version includes an auto-updater that will upgrade it to the latest version automatically. To update the GIT version run `git pull` inside the repository instead.

# Support #
Forum thread: http://forum.projectgorgon.com/showthread.php?125-Project-Gorgon-on-Linux&p=1728&viewfull=1#post1728<br />
Issue tracker: https://gitlab.com/alexander255/project-gorgon-launcher/issues

# Usage #
When running `project-gorgon` without arguments it will automatically start
a terminal emulator (if none was provided) and download the game.

When running the launcher with the `-p windows` flag from an existing terminal
window it will download the Windows game client into the current WINEPREFIX and
run it through WINE.

# Compatibility #
This launcher has been tested and is working on Ubuntu, Debian and Tanglu Linux
by its author. Users have also reported that it works well on Arch Linux and
Manjaro. 

In theory this launcher should also work on Windows but this has not been tested
and problems for this configuration will only be addressed if there is actual
interest by anybody to use it this way.