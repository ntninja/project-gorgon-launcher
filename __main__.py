# Main entry point for bundled python executable version

import os.path
import sys

# Add bundled dependencies to path (but prefer distribution versions)
sys.path.append("{0}/deps".format(os.path.dirname(__file__)))

import pglauncher.main
pglauncher.main.main()
