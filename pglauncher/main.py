#!/usr/bin/python3

#########################################################################
# pglauncher - An unoffical Launcher for the "Project: Gorgon" game     #
# Copyright (C) 2014  Alexander Schlarb                                 #
#                                                                       #
# This program is free software: you can redistribute it and/or modify  #
# it under the terms of the GNU General Public License as published by  #
# the Free Software Foundation, either version 3 of the License, or     #
# (at your option) any later version.                                   #
#                                                                       #
# This program is distributed in the hope that it will be useful,       #
# but WITHOUT ANY WARRANTY; without even the implied warranty of        #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
# GNU General Public License for more details.                          #
#########################################################################


import argparse
import importlib
import sys
import os

def main(argv=sys.argv[1:], program=sys.argv[0]):
	try:
		parser = argparse.ArgumentParser()
		parser.add_argument("--ui", "-u", dest="ui", action="append", default=[],
				help="The user interface to use (default: tk, cli)")
		parser.add_argument("--fileversion", "-fileversion",
				dest="game_version", type=str, default=None,
				help="Attempt to download a specific version of the game instead of "
				     "using the most recent stable version published by the server")
		parser.add_argument("--game-path", dest="game_path", type=str, default=None,
				help="game path, default: ~/.local/games/project-gorgon")
		args, args_unknown = parser.parse_known_args()
		
		# Apply data directory set from command-line
		if args.game_path:
			os.environ["PROJECT_GORGON_DATA_HOME"] = args.game_path
		
		# Load requested user interface
		ui = None
		if not args.ui:
			args.ui = ["tk", "cli"]
		for ui_name in args.ui:
			try:
				ui = importlib.import_module("pglauncher.ui.{}.main".format(ui_name)).UI()
				break
			except ImportError as e:
				print("User interface \"{}\" not available".format(ui_name))
		if not ui:
			parser.error("No suitable user interface found")
		
		# Show user interface
		ui.run(args_unknown, game_version=args.game_version)
	except KeyboardInterrupt:
		print()  # Hide stack trace on keyboard interrupt


if __name__ == '__main__':
	main()
