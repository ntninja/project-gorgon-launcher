"""

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

------------------------------------------------------------------------
Update 2016/07/05:
- Fixed an error that would cause the launcher crash.
- Updated patcher urls.
- Added the welcome message to debug text window.

------------------------------------------------------------------------
Project Gorgon: Linux Launcher

1) Description

This program is a GNU/Linux GUI launcher for the game ProjectGorgon. It
will get the launcher version, game version, game manifest, patch game
data, and launch the game. There is a means to validate the game data by
checking modification date and file size, or by using an md5 checksum.
The program does create a checksum file of the manifest to speed up the
patching process.

2) Required Python Modules

- pyttk
- requests

  (Use pip for installation)

3) Notes

- default game path for launcher.
  path: ~/Games/ProjectGorgon (can be changed with program argument)
  
- manifest checksum file.
  file: manifest.checksum

- the binary file will be given proper permissions, if necessary.
  permissions: 755 (rwx|r x|r x)

4) Troubleshoot

- Launcher does not patch on start.
+ Deleting the manifest.checksum file will force the launcher to patch
  on start.

- Launcher crashes.
+ Not all cases of error exception is in place, the program traceback 
  should print out to the terminal, please contact the author.

- Game crashes or fails to start.
+ Validate game data by clicking the Validate Game Data button in the 
  graphcial user interface.

5) Author/Contact

- ProjectGorgon User: MFsorc (http://projectgorgon.com/forum/user/6777-mfsorc)

6) Contribution/Thanks

- ProjectGorgon User: shardragon
  ProjectGorgon Post: http://projectgorgon.com/forum/support/77-project-gorgon-on-linux?limitstart=0#2760
  Contribution: PGLauncher patcher.

"""

import argparse
import logging
import platform
import os
import sys

import pglauncher.patcher
import pglauncher.launcher

from . import gui


class UI:
	def run(self, argv, **patcher_kwargs):
		root = gui.tkinter.Tk()
		
		window = gui.Launcher(root, **patcher_kwargs)
		window.do_patch()
		
		root.mainloop()
