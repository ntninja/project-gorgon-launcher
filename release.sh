#!/bin/sh
set -e

read -p "Enter version number: " VERSION

DIST_DIR="pglauncher-${VERSION}.tmp"
DIST_ZIP="pglauncher-${VERSION}.zip"
DIST_EXE="pglauncher-${VERSION}"

# Change working directory to script directory
cd "`dirname "$(readlink -f "${0}")"`"
OLDPWD="$(pwd)"

# Make sure that Python 3 PIP is available
if ! type pip3 2>/dev/null >&2;
then
	echo "This script requires the Python 3 PIP command (\`pip3\`)" >&2
	exit 2
fi

# Make sure that GIT is available
if ! type git 2>/dev/null >&2;
then
	echo "This script required the GIT command (\`git\`)" >&2
	exit 2
fi

if ! [ "${1}" = "--test" ];
then
	# Make sure there are no uncommited changes
	if [ -n "`git status -s`" ];
	then
		echo "You have uncommited changes:" >&2
		git status >&2
		echo 'Please commit your changes or run with `--test` to create a testing release instead!' >&2
		exit 3
	fi
	
	# Tag latest commit
	git tag -f "v${VERSION}"
fi

# Create release file directory
mkdir "${DIST_DIR}"

# Add file with latest version to release
echo "${VERSION}" >"${DIST_DIR}/VERSION.txt"

# Copy files to release directory
echo
echo "+–––––––––––––––+"
echo "| Copying files |"
echo "+–––––––––––––––+"
git ls-tree --name-only -r HEAD | grep -vE '.gitignore$' | grep -vx "`basename "${0}"`" | while read filepath;
do
	mkdir -vp "${DIST_DIR}/`dirname "${filepath}"`"
	cp -v "${filepath}" "${DIST_DIR}/${filepath}"
done

# Download dependencies into release directory
echo
echo "+––––––––––––––––––––––––––+"
echo "| Downloading dependencies |"
echo "+––––––––––––––––––––––––––+"
mkdir "${DIST_DIR}/deps"
if [ -e /etc/debian_version ];
then
	pip3 install --system --target "${DIST_DIR}/deps" --no-compile requests
else
	pip3 install --target "${DIST_DIR}/deps" --no-compile requests
fi

# Create archive from release directory
echo
echo "+–––––––––––––––––––––+"
echo "| Creating executable |"
echo "+–––––––––––––––––––––+"
rm -v "${DIST_EXE}" 2>/dev/null ||:
cd "${DIST_DIR}"
zip -r "../${DIST_ZIP}" *
cd "${OLDPWD}"

echo "#!/usr/bin/python3" > "${DIST_EXE}"
cat "${DIST_ZIP}"        >> "${DIST_EXE}"
chmod +x "${DIST_EXE}"

# Remove release directory
echo
echo "+–––––––––––––+"
echo "| Cleaning up |"
echo "+–––––––––––––+"
rm -v  "${DIST_ZIP}"
rm -vr "${DIST_DIR}/"

if ! [ "${1}" = "--test" ];
then
	# Add to the IPFS file repository
	echo
	echo "+–––––––––––––-----------+"
	echo "| Publishing new release |"
	echo "+–––––––––––––-----------+"
	cat "${DIST_EXE}"    | ipfs files write -te "/Software/Python/pglauncher/pglauncher"
	cat "${DIST_EXE}"    | ipfs files write -te "/Software/Python/pglauncher/pglauncher-${VERSION}.bin"
	echo -n "${VERSION}" | ipfs files write -te "/Software/Python/pglauncher/version.txt"
	ipfs name publish "$(ipfs files stat --hash "/")"
fi
